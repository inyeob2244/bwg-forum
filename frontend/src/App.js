import './App.css';
import Contributor from './Layouts/Contributor';
import Header from './Layouts/Header';
import Main from './Layouts/Main';
import Footer from './Layouts/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
      <Contributor />
      <Footer />
    </div>
  );
}

export default App;
