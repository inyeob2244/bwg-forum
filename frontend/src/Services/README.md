## 서비스 폴더

https://ko.redux.js.org/introduction/getting-started/

이 폴더는 프로젝트에서 Redux를 사용하는 경우에 추가됩니다. 이 폴더 안에는 상태를 관리하기 위한 actions, reducers 및 상수 하위 폴더가 3개 포함되어 있습니다. actions 및 reducers는 거의 모든 페이지에서 호출되므로 페이지 이름에 따라 actions, reducers 및 constants를 생성하세요.