## 유틸 폴더

유틸 폴더에는 프로젝트에서 일반적으로 사용되는 여러 번 반복되는 함수가 포함되어 있습니다. 여기에는 드롭다운 옵션, 정규식 조건, 데이터 형식 지정 등과 같은 공통된 JavaScript 함수 및 객체만 포함되어야 합니다.